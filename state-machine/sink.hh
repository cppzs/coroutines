// simple coroutine sink
#ifndef SINK_HH_INCLUDED
#define SINK_HH_INCLUDED

#include <cppcoro/coroutine.hpp>

namespace cppcoro
{
template <typename T>
class sink
{
    struct sink_promise
    {
        suspend_never initial_suspend() noexcept
        {
            return {};
        }

        suspend_never final_suspend () noexcept
        {
            return {};
        }

        suspend_always await_transform(T &val) noexcept
        {
            next_value = &val;
            return {};
        }

        sink get_return_object() noexcept
        {
            return sink{coroutine_handle<sink_promise>::from_promise(*this)};
        }

        void return_void()
        {
        }

        void unhandled_exception() noexcept
        {
        }

        T *next_value;
    };

public:
    typedef sink_promise promise_type;

    ~sink()
    {
        if (coro)
        {
            coro.destroy();
        }
    }

    void operator()(T val)
    {
        *(coro.promise().next_value) = val;
        coro.resume();
    }

private:
    sink(coroutine_handle<sink_promise> h) : coro{h} {}

    coroutine_handle<sink_promise> coro;
};
} // namespace cppcoro
#endif /* SINK_HH_INCLUDED */
