// implementating a simple state machine

#include <iostream>
#include <fstream>
#include <string>

class MessageReader
{
public:
    template <class F>
    MessageReader(F callback)
      : processMsg{callback}
    {
    }

    void nextEntry(int c)
    {
        switch (st)
        {
        case InMessage:
            switch (c)
            {
            case EOF:
                processMsg(msg);
                break;

            case '\n':
                st = SingleNL;
                break;

            default:
                msg += c;
            }

            break;

        case SingleNL:
            switch (c)
            {
            case EOF:
                processMsg(msg);
                break;

            case '\n':
                processMsg(msg);
                msg = "";
                st = InMessage;
                break;

            default:
                msg += '\n';
                msg += c;
                st = InMessage;
            }

            break;
        }
    }

private:
    enum State { InMessage, SingleNL };
    State st = InMessage;
    std::string msg;
    std::function<void(std::string_view)> processMsg;
};

template <class F>
void readMessages(std::ifstream &in, F processMsg)
{
    MessageReader sm{processMsg};

    while (!in.eof())
    {
        auto c = in.get();
        sm.nextEntry(c);
    }
}

#include "main.cc"
