#include <fstream>
#include <iostream>
#include <string_view>

int main()
{
    std::ifstream in{"testdat.txt"};

    readMessages(in,
                 [] (std::string_view msg)
                 {
                     std::cout << "Got message: " << msg << '\n';
                 });

    return 0;
}
