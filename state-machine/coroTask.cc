// state machine using coroutine and task
#include <string>
#include <iostream>
#include <fstream>

#include <cppcoro/task.hpp>
#include <cppcoro/sync_wait.hpp>

cppcoro::task<int> readChar(std::ifstream &in)
{
    co_return in.get();
}

template <class F>
cppcoro::task<void> messageReader(std::ifstream &in, F processMsg)
{
    std::string msg;
    bool stop = false;

    while (!stop)
    {
        int c = co_await readChar(in);

        switch (c)
        {
        case EOF:
            processMsg(msg);
            stop = true;
            break;

        case '\n':
        {
            int next = co_await readChar(in);
            switch (next)
            {
            case EOF:
                processMsg(msg);
                stop = true;
                break;

            case '\n':
                processMsg(msg);
                msg = "";
                break;

            default:
                msg += c;
                msg += next;
            }
        }
            break;

        default:
            msg += c;
        }
    }
}

template <class F>
void readMessages(std::ifstream &in, F processMsg)
{
    auto reader = messageReader(in, processMsg);
    cppcoro::sync_wait(reader);
}

#include "main.cc"
