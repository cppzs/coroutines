// state machine using coroutine
#include <string>
#include <iostream>
#include <fstream>

#include "sink.hh"

template <class F>
cppcoro::sink<int> messageReader(F processMsg)
{
    std::string msg;

    while (true)
    {
        int c;
        co_await c;

        switch (c)
        {
        case EOF:
            processMsg(msg);
            break;

        case '\n':
        {
            int next;
            co_await next;
            switch (next)
            {
            case EOF:
                processMsg(msg);
                break;

            case '\n':
                processMsg(msg);
                msg = "";
                break;

            default:
                msg += c;
                msg += next;
            }
        }
            break;

        default:
            msg += c;
        }
    }
}

template <class F>
void readMessages(std::ifstream &in, F processMsg)
{
    auto reader = messageReader(processMsg);

    while (!in.eof())
    {
        auto c = in.get();
        reader(c);
    }
}

#include "main.cc"
