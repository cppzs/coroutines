// a simple message reader

#include <iostream>
#include <fstream>
#include <string>

template <class F>
void readMessages(std::ifstream &in, F processMsg)
{
    std::string msg;

    while (!in.eof())
    {
        auto c = in.get();

        switch (c)
        {
        case EOF:
            processMsg(msg);
            break;

        case '\n':
        {
            auto next = in.get();
            switch (next)
            {
            case EOF:
                processMsg(msg);
                break;

            case '\n':
                processMsg(msg);
                msg = "";
                break;

            default:
                msg += c;
                msg += next;
            }
        }
        break;

        default:
            msg += c;
        }
    }
}

#include "main.cc"
