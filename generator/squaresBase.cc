// simple example to produce square numbers

#include <iostream>

void printSquares(int n)
{
    for (int i = 0; i != n; ++i)
    {
        std::cout << i*i << '\n';
    }
}

int main()
{
    printSquares(10);

    return 0;
}
