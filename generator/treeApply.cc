#include <vector>
#include <iostream>

class Tree
{
    struct Node;

public:
    Node *addChild(Node *node, int value)
    {
        node->children.push_back(Node{value, {}});
        return &(node->children.back());
    }

    Node *getRoot()
    {
        return &root;
    }

    Node *find(int value)
    {
        return find(&root, value);
    }

    template <class F>
    void apply(F foo)
    {
        apply(&root, foo);
    }

private:
    struct Node
    {
        int val;
        std::vector<Node> children;
    };

    Node *find(Node *start, int value)
    {
        if (start->val == value)
        {
            return start;
        }
        for (Node &n: start->children)
        {
            Node *found = find(&n, value);
            if (found)
            {
                return found;
            }
        }
        return nullptr;
    }

    template <class F>
    void apply(Node *start, F foo)
    {
        foo(start->val);
        for (Node &n: start->children)
        {
            apply(&n, foo);
        }
    }

    Node root{0, {}};
};

#include "treeBench.hh"

void runBenchmark(Tree t)
{
    startTimer();

    t.apply([] (int &v) { ++v; });
    std::cout << "increment uS: " << timerUs() << '\n';

    long long sum = 0;
    t.apply([&sum] (int v) { sum += v; });
    std::cout << "sum: " << sum << " uS: " << timerUs() << '\n';
}

int main()
{
    Tree t;
    auto n1 = t.addChild(t.getRoot(), 1);
    t.addChild(n1, 11);
    t.addChild(n1, 12);
    auto n2 = t.addChild(t.getRoot(), 2);
    t.addChild(n2, 21);
    auto n22 = t.addChild(n2, 22);
    t.addChild(n22, 221);

    t.apply([] (int v) { std::cout << v << '\n'; });

    t.apply([] (int &v) { ++v; });
    t.apply([] (int v) { std::cout << v << '\n'; });

    std::cout << "Degenerate tree:\n";
    runBenchmark(makeDegTree(10'000));
    std::cout << "Random tree:\n";
    runBenchmark(makeRandTree(100'000));

    return 0;
}
