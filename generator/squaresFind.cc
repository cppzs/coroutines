// simple example to produce square numbers

#include <iostream>

int digSum(int i)
{
    if (i < 0)
    {
        i = -i;
    }
    int sum = i % 10;
    while ((i /= 10) > 0)
    {
        sum += i % 10;
    }
    return sum;
}

template <class F>
void fooForSquares(F foo, int n)
{
    for (int i = 0; i != n; ++i)
    {
        foo(i*i);
    }
}

int main()
{
    fooForSquares([] (int s)
                  {
                      if (digSum(s) == 18)
                      {
                          std::cout << s << '\n';
                      }
                  }, 10000);

    return 0;
}
