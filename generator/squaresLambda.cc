// simple example to produce square numbers

#include <iostream>

template <class F>
void fooForSquares(F foo, int n)
{
    for (int i = 0; i != n; ++i)
    {
        foo(i*i);
    }
}

int main()
{
    fooForSquares([] (int s) { std::cout << s << '\n'; }, 10);

    return 0;
}
