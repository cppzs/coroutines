// simple example to produce square numbers

#include <iostream>

int digSum(int i)
{
    if (i < 0)
    {
        i = -i;
    }
    int sum = i % 10;
    while ((i /= 10) > 0)
    {
        sum += i % 10;
    }
    return sum;
}

template <class F>
void fooForSquares(F foo, int n)
{
    for (int i = 0; i != n; ++i)
    {
        foo(i*i);
    }
}

int main()
{
    fooForSquares([] (int s) { std::cout << digSum(s) << '\n'; }, 10);

    return 0;
}
