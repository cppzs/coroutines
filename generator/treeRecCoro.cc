#include <vector>
#include <deque>
#include <iostream>
#include <cppcoro/recursive_generator.hpp>

class Tree
{
    struct Node;
    struct Iter;

public:
    Node *addChild(Node *node, int value)
    {
        node->children.push_back(Node{value, {}});
        return &(node->children.back());
    }

    Node *getRoot()
    {
        return &root;
    }

    cppcoro::recursive_generator<int> range()
    {
        return range(&root);
    }

    Node *find(int value)
    {
        return find(&root, value);
    }

    template <class F>
    void apply(F foo)
    {
        apply(&root, foo);
    }

private:
    struct Node
    {
        int val;
        std::vector<Node> children;
    };

    cppcoro::recursive_generator<int> range(Node *start)
    {
        co_yield start->val;
        for (Node &n: start->children)
        {
            co_yield range(&n);
        }
    }

    Node *find(Node *start, int value)
    {
        if (start->val == value)
        {
            return start;
        }
        for (Node &n: start->children)
        {
            Node *found = find(&n, value);
            if (found)
            {
                return found;
            }
        }
        return nullptr;
    }

    template <class F>
    void apply(Node *start, F foo)
    {
        foo(start->val);
        for (Node &n: start->children)
        {
            apply(&n, foo);
        }
    }

    Node root{0, {}};
};

#include "treeBench.hh"

void runBenchmark(Tree t)
{
    startTimer();

    for (int &v: t.range())
    {
        ++v;
    }
    std::cout << "increment uS: " << timerUs() << '\n';

    long long sum = 0;
    for (int v: t.range())
    {
        sum += v;
    }
    std::cout << "sum: " << sum << " uS: " << timerUs() << '\n';
}

int main()
{
    Tree t;
    auto n1 = t.addChild(t.getRoot(), 1);
    t.addChild(n1, 11);
    t.addChild(n1, 12);
    auto n2 = t.addChild(t.getRoot(), 2);
    t.addChild(n2, 21);
    auto n22 = t.addChild(n2, 22);
    t.addChild(n22, 221);

    for (int v: t.range())
    {
        std::cout << v << '\n';
    }

    for (int &v: t.range())
    {
        ++v;
    }

    for (int v: t.range())
    {
        std::cout << v << '\n';
    }

    std::cout << "Degenerate tree:\n";
    runBenchmark(makeDegTree(10'000));
    std::cout << "Random tree:\n";
    runBenchmark(makeRandTree(100'000));

    return 0;
}
