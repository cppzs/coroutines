// simple example to produce square numbers

#include <algorithm>
#include <iostream>
#include <cppcoro/generator.hpp>

int digSum(int i)
{
    if (i < 0)
    {
        i = -i;
    }
    int sum = i % 10;
    while ((i /= 10) > 0)
    {
        sum += i % 10;
    }
    return sum;
}

cppcoro::generator<const int> squares()
{
    int i = 0;
    while (true)
    {
        co_yield i*i;
        ++i;
    }
}

int main()
{
    auto sqGen = squares();
    for (auto it = sqGen.begin(); it != sqGen.end(); ++it)
    {
        if (digSum(*it) == 18)
        {
            std::cout << *it << '\n';
            break;
        }
    }
    std::cout << *(sqGen.begin()) << '\n';

    return 0;
}
