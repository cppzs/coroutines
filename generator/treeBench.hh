
#include <chrono>
#include <cstdlib>

std::chrono::high_resolution_clock::time_point start;

Tree makeDegTree(unsigned int num)
{
    // populate degenerate tree
    Tree t;
    auto n = t.getRoot();
    for (int i = 0; i != num; ++i)
    {
        n = t.addChild(n, i);
    }

    return t;
}

int curVal = 0;

int makeRandSubTree(Tree &t, decltype(t.getRoot()) n, int level, int num)
{
    if (num <= 0) return 0;
    n = t.addChild(n, curVal*1000 + level);
    ++curVal;
    int maxChild = 35 - level;
    if (maxChild < 2) maxChild = 2;
    int childNum = random() % maxChild;
    if (level == 0 && childNum == 0) childNum = 2;

    int count = 1;
    for (int i = 0; i != childNum; ++i)
    {
        if (num <= count) break;
        int nodeNum = random() % (((num - count) * 2) / (childNum - i) + 1);
        count += makeRandSubTree(t, n, level + 1, nodeNum);
    }

    return count;
}

Tree makeRandTree(int num)
{
    // populate random tree
    curVal = 0;
    Tree t;
    auto n = t.getRoot();
    makeRandSubTree(t, n, 0, num);

    return t;
}

void startTimer()
{
    start = std::chrono::high_resolution_clock::now();
}

int timerUs()
{
    using namespace std::chrono;
    auto dur = high_resolution_clock::now() - start;
    return duration_cast<microseconds>(dur).count();
}
