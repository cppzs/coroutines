#include <vector>
#include <deque>
//#include <list>
#include <iostream>

class Tree
{
    struct Node;
    struct Iter;

public:
    Node *addChild(Node *node, int value)
    {
        node->children.push_back(Node{value, {}});
        return &(node->children.back());
    }

    Node *getRoot()
    {
        return &root;
    }

    Iter begin()
    {
        return {&root};
    }

    Iter end()
    {
        return {};
    }

    Node *find(int value)
    {
        return find(&root, value);
    }

    template <class F>
    void apply(F foo)
    {
        apply(&root, foo);
    }

private:
    struct Node
    {
        int val;
        std::vector<Node> children;
    };

    struct Iter : public std::iterator <std::forward_iterator_tag, int>
    {
        Iter() = default;
        Iter(Node *start) { stack.push_front(StackItem{start}); }
        int &operator*() { return stack.front().elem->val; }
        int *operator->() { return &stack.front().elem->val; }
        Iter &operator++()
        {
            while (nodeDone())
            {
                stack.pop_front();
            }
            if (stack.empty())
            {
                return *this;
            }
            StackItem &top = stack.front();
            if (top.curChild == top.elem->children.end())
            {
                top.curChild = top.elem->children.begin();
            }
            else
            {
                // next child
                ++(top.curChild);
            }

            // go down
            stack.push_front(StackItem{&(*top.curChild)});

            return *this;
        }
        Iter operator++(int)
        {
            Iter old{*this};
            operator++();
            return old;
        }
        bool operator==(const Iter &rhs) const
        { return stack == rhs.stack; }
        bool operator!=(const Iter &rhs)const
        { return stack != rhs.stack; }

        bool nodeDone()
        {
            if (stack.empty()) return false;
            StackItem const &top = stack.front();
            if (top.elem->children.empty()) return true;
            if (top.curChild + 1 == top.elem->children.end()) return true;
            return false;
        }

        struct StackItem
        {
            StackItem(Node *n) : elem(n), curChild(n->children.end()) {}
            bool operator==(StackItem const &) const = default;
            Node *elem;
            std::vector<Node>::iterator curChild;
            bool childrenStarted = false;
        };
        std::deque<StackItem> stack;
        //std::list<StackItem> stack;
    };

    Node *find(Node *start, int value)
    {
        if (start->val == value)
        {
            return start;
        }
        for (Node &n: start->children)
        {
            Node *found = find(&n, value);
            if (found)
            {
                return found;
            }
        }
        return nullptr;
    }

    template <class F>
    void apply(Node *start, F foo)
    {
        foo(start->val);
        for (Node &n: start->children)
        {
            apply(&n, foo);
        }
    }

    Node root{0, {}};
};

#include "treeBench.hh"

void runBenchmark(Tree t)
{
    startTimer();

    for (int &v: t)
    {
        ++v;
    }

    std::cout << "increment uS: " << timerUs() << '\n';

    long long sum = 0;

    for (int &v: t)
    {
        sum += v;
    }
    std::cout << "sum: " << sum << " uS: " << timerUs() << '\n';
}

int main()
{
    Tree t;
    auto n1 = t.addChild(t.getRoot(), 1);
    t.addChild(n1, 11);
    t.addChild(n1, 12);
    auto n2 = t.addChild(t.getRoot(), 2);
    t.addChild(n2, 21);
    auto n22 = t.addChild(n2, 22);
    t.addChild(n22, 221);

    for (int v: t)
    {
        std::cout << v << '\n';
    }

    for (int &v: t)
    {
        ++v;
    }

    for (int v: t)
    {
        std::cout << v << '\n';
    }

    std::cout << "Degenerate tree:\n";
    runBenchmark(makeDegTree(10'000));
    std::cout << "Random tree:\n";
    runBenchmark(makeRandTree(100'000));

    return 0;
}
