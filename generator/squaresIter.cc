// simple example to produce square numbers

#include <iterator>
#include <algorithm>
#include <iostream>

int digSum(int i)
{
    if (i < 0)
    {
        i = -i;
    }
    int sum = i % 10;
    while ((i /= 10) > 0)
    {
        sum += i % 10;
    }
    return sum;
}

class SquareIter : public std::iterator <std::input_iterator_tag, int>
{
public:
    SquareIter() : i(0), s(0) {}
    explicit SquareIter(int n) : i(n), s(0) {}
    int operator*() const { return s; }
    int const *operator->() const { return &s; }
    const SquareIter &operator++()
    {
        ++i;
        s = i*i;
        return *this;
    }
    void operator++(int)
    {
        return (void)operator++();
    }
    bool operator==(const SquareIter &rhs) const
    { return i == rhs.i; }
    bool operator!=(const SquareIter &rhs)const
    { return i != rhs.i; }

private:
    int i;
    int s;
};

int main()
{
    auto found = std::find_if(SquareIter(), SquareIter(10000),
                              [] (int s) { return digSum(s) == 18;});
    std::cout << *found << '\n';

    return 0;
}
