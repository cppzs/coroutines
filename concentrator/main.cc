#include "timer.hh"
#include "epollLoop.hh"
#include "coQueue.hh"

#include <iostream>

class DummyPromise;
struct CoHandle
{
    typedef DummyPromise promise_type;
    typedef std::experimental::coroutine_handle<promise_type> CoHandleT;

    ~CoHandle()
    {
        handle.destroy();
    }

    CoHandleT handle;
};

class DummyPromise
{
    typedef std::experimental::suspend_always Suspend;
    typedef std::experimental::suspend_never NoSuspend;
public:
    NoSuspend initial_suspend()
    {
        return {};
    }
    NoSuspend final_suspend() noexcept
    {
        return {};
    }
    void return_void()
    {
    }
    CoHandle get_return_object()
    {
        return {CoHandle::CoHandleT::from_promise(*this)};
    }
    void unhandled_exception() noexcept
    {
    }
};


struct DataT
{
    int id;
    uint64_t data;
};

EpollLoop evLoop;

CoHandle read1(CoQueue<DataT> &q)
{
    // starts the reader (timer for our demo)
    TimerAwaitable read1{evLoop, itimerspec{{1, 500'000'000}, {1, 500'000'000}}};

    while (1)
    {
        uint64_t dummy = co_await read1; // wait for next item to arrive
        // now create a value to push
        DataT val = { 1, dummy };
        co_await q.push(val);
    }
}

CoHandle read2(CoQueue<DataT> &q)
{
    // starts the reader (timer for our demo)
    TimerAwaitable read2{evLoop, itimerspec{{1, 0}, {1, 0}}};

    while (1)
    {
        uint64_t dummy = co_await read2; // wait for next item to arrive
        // now create a value to push
        DataT val = { 2, dummy };
        co_await q.push(val);
    }
}

CoHandle log(CoQueue<DataT> &q)
{
    int count = 0;
    while (1)
    {
        DataT v = co_await q.pull();
        std::clog << "Data from " << v.id << ": " << v.data << '\n';
        ++count;
        if (count == 10)
        {
            evLoop.stop();
        }
    }
}


int main()
{
    CoQueue<DataT> queue;

    auto coro1 = read1(queue); // start producer 1
    auto coro2 = read2(queue); // start producer 2
    auto coro3 = log(queue);   // start consumer

    evLoop.loop<TimerAwaitable>(); // event loop

    return 0;
}
