#ifndef CO_QUEUE_HH_INCLUDED
#define CO_QUEUE_HH_INCLUDED
#include <experimental/coroutine>

template <typename T>
class CoQueue
{
    typedef std::experimental::coroutine_handle<void> CoHandleT;
public:
    class PushAwaiter
    {
    public:
        PushAwaiter(CoQueue *q_)
          : q{q_}
        {
        }

        bool await_ready() const noexcept
        {
            return false;
        }

        void await_suspend(CoHandleT) noexcept
        {
            // resume reader
            if (q->consumer)
            {
                q->consumer();
            }
        }

        void await_resume() noexcept
        {
        }

    private:
        CoQueue<T> *q;
    };

    class PullAwaiter
    {
    public:
        PullAwaiter(CoQueue *q_)
          : q{q_}
        {
        }

        bool await_ready() const noexcept
        {
            return q->full;
        }

        void await_suspend(CoHandleT coro) noexcept
        {
            q->consumer = coro;
        }

        T await_resume()
        {
            // this only works for a push-driven queue
            // otherwise we would need to store the producer handle
            // to resume here, which is a problem for multiple producers
            if (!q->full)
            {
                throw std::runtime_error("Resuming on empty queue doesn't work");
            }
            q->full = false;
            return q->data;
        }

    private:
        CoQueue<T> *q;
    };

    PushAwaiter push(T val)
    {
        data = val;
        full = true;
        return {this};
    }

    PullAwaiter pull()
    {
        return {this};
    }

private:
    CoHandleT consumer;
    T data;
    bool full = false;
};
#endif /* CO_QUEUE_HH_INCLUDED */
