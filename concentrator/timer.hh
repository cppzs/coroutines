#ifndef TIMER_HH_INCLUDED
#define TIMER_HH_INCLUDED

#include <sys/timerfd.h>
#include <unistd.h>
#include "epollLoop.hh"

#include <experimental/coroutine>
#include <stdexcept>
#include <cstdint>

class TimerAwaitable
{
    typedef std::experimental::coroutine_handle<> coroHandleT;
public:
    TimerAwaitable(EpollLoop &ep, itimerspec tSpec)
      : fd{timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK)}
    {
        if (fd < 0)
        {
            throw std::runtime_error("Couldn't create timerfd");
        }
        // start timer
        if (timerfd_settime(fd, 0, &tSpec, 0) < 0)
        {
            throw std::runtime_error("Couldn't settime for timerfd");
        }
        // register for epoll
        ep.add(*this);
    }

    bool await_ready() const noexcept
    {
        return false;
    }

    bool await_suspend(coroHandleT self) noexcept
    {
        coro = self;

        // we would start the next interval here
        // but we have a self-repeating timer

        return true; // stay suspended
    }

    uint64_t await_resume() noexcept
    {
        return timeValue;
    }

    void handleEvent()
    {
        // read the (dummy) bytes from the timer
        size_t cnt = ::read(fd, &timeValue, sizeof(timeValue));
        // now resume ourselves
        coro();
    }

private:
    friend class EpollLoop;

    coroHandleT coro;
    uint64_t timeValue;
    int fd;
};
#endif /* TIMER_HH_INCLUDED */
