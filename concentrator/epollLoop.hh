#ifndef EPOLL_LOOP_HH_INCLUDED
#define EPOLL_LOOP_HH_INCLUDED

#include <sys/epoll.h>
#include <unistd.h>

#include <stdexcept>

class EpollLoop
{
public:
    EpollLoop()
      : fd{epoll_create(1)}
    {
        if (fd < 0)
        {
            throw std::runtime_error("Couldn't create epollfd");
        }
    }

    template <class EpollAwaitable>
    void add(EpollAwaitable &aw)
    {
        epoll_event ev = {EPOLLIN, {&aw}};
        if (epoll_ctl(fd, EPOLL_CTL_ADD, aw.fd, &ev) < 0)
        {
            throw std::runtime_error("Couldn't add timerfd to epoll");
        }
    }

    template <class EpollAwaitable>
    void loop()
    {
        epoll_event waitEv;
        while (!doStop)
        {
            int cnt = epoll_wait(fd, &waitEv, 1, -1);
            if (cnt > 0)
            {
                EpollAwaitable *src
                    = static_cast<EpollAwaitable *>(waitEv.data.ptr);
                src->handleEvent();
            }
        }
    }

    void stop()
    {
        doStop = true;
    }

private:
    int fd;
    bool doStop = false;
};
#endif /* EPOLL_LOOP_HH_INCLUDED */
