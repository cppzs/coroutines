main
  read1 init
    create read1 timer awaitable
    co_await -> suspend
  read1 returns

  read2 init
    create read2 timer awaitable
    co_await -> suspend
  read2 returns

  log init
    co_await -> suspend
  log returns

  loop
    epoll (blocks)
    timer.handleEvent
      read2 resume
        q.push
        co_await -> suspend read2
          resume log
          co_await -> suspend log
      return to timer.handleEvent
    timer.handleEvent returns
    back to epoll
